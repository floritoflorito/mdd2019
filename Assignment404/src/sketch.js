var images = [ [], [], [] ];
var row=1, col=1;
var x0,x1,y0,y1;
var img;

var myText="ERROR 404!!!      ERROR 404!!!      Where is that bloody page??"
var textStart=0;
var scrolledText=0;
var totalTextLength = myText.length+50;

var urlText;

console.log(totalTextLength);

function setup() {
  let cnv=createCanvas(960/3, 1280/3);
  cnv.parent("canvasContainer");
  
  window.history.replaceState(null, "Error 404", "/");//"object or string"
  //body=
  
  x0 = width*0.1;
  x1 = width-x0;
  y0 = height*0.1;
  y1 = height-y0;
  
  // load images
  var nr=5070;
  for (var row=0;row<=2;row++) {
    for (var col=0;col<=2;col++) {
      let name='./assets/IMG_'+nr+'.JPG';
      console.log("loading "+name);
      images[row][col] = loadImage(name);
      nr++;
    }
  }
  img = images[1][1];
  bgcol = [215,229,84];
}

function draw() {

	//console.log(frameCount);
	
	if (frameCount%3==0) {
		
		// make text like so: _______Hell

		if (textStart<myText.length) {
			urlText="";
			urlText = myText.substring(0,textStart);
			let addedChars = totalTextLength-urlText.length;
			for (let i=0;i<addedChars;i++) {
				urlText = "_"+urlText;
			}
			urlText = urlText.replace(/ /g, "_");
			textStart++;
			scrolledText=0;
		}

		// scroll text left
		else if (scrolledText<totalTextLength) {
			urlText = urlText.substring(1)+"_";
			scrolledText++;
			if (scrolledText==totalTextLength) {
				textStart=0;
			}
		}


		//window.history.pushState("object or string", "Title", "/");
		window.history.replaceState("object or string", "Title", "/"+urlText);
	}
  
  if (mouseX<x0) { col=2; }
  else if (mouseX>x1) { col=0; }
  else { col=1; }
  
  if (mouseY<y0) { row=0; }
  else if (mouseY>y1) { row=2; }
  else { row=1; }
  
  img = images[row][col];
  

  let bgcol=img.get(img.width/2,20);
  var r= bgcol[0];    
  var g= bgcol[1];
  var b= bgcol[2];
  document.body.style="background-color: rgb("+r+","+g+","+b+");";

  
  
  background(0);
  //scale(1/3);
  translate(width,0);
  rotate(HALF_PI);
  scale(1/3);
  image(img,0,0);
  
  lastImage = img;
}