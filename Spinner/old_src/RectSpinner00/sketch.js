var rectLeft, rectTop, rectWidth, rectHeight;
var rectRight, rectBottom;
var ballDiameter;
var ballsHorizontal;
var ballsVertical;
var ballsTotal;

function setup() {
  createCanvas(400, 600);
  
  // initialize rectangle area for spinner
  rectWidth = width/2;
  rectHeight = height/2;
  rectLeft = width/2 - rectWidth/2;
  rectTop = height/2 - rectHeight/2;
  rectRight = rectLeft+rectWidth;
  rectBottom = rectTop+rectHeight;
  
  // initialize ball size
  ballDiameter = rectWidth*0.10;
  
  // initialize horizontal/vertical amount of balls
  ballsHorizontal = floor(rectWidth/ballDiameter);
  ballsVertical = floor(rectHeight/ballDiameter);
  ballsTotal = 2*ballsHorizontal+2*ballsVertical-4;
}

function draw() {
  background(0);
  
  // my own function for drawing the test rectangle and a test ball
  drawTest();
  
  // draw all balls clockwise
  var ballIndex=0;
  // draw balls top row (left to right)
  for (var i=0;i<ballsHorizontal;i++) {
    let x = map(i,0,ballsHorizontal-1, rectLeft, rectRight);
    drawBall(x,rectTop,ballIndex);
    ballIndex++;
  }
  
  // draw balls right side (top to bottom)
  for (i=1;i<ballsVertical;i++) {
    let y = map(i,0,ballsVertical-1, rectTop, rectBottom);
    drawBall(rectRight,y,ballIndex);
    ballIndex++;
  }
  
  // draw balls bottom row (right to left)
  for (var i=ballsHorizontal-2;i>=0;i--) {
    let x = map(i,0,ballsHorizontal-1, rectLeft, rectRight);
    drawBall(x,rectBottom,ballIndex);
    ballIndex++;
  }
  
  // draw balls left side (bottom to top)
  for (i=ballsVertical-2;i>0;i--) {
    let y = map(i,0,ballsVertical-1, rectTop, rectBottom);
    drawBall(rectLeft,y,ballIndex);
    ballIndex++;
  }
  
}


// test for drawing ball and rect
function drawTest() {
  // test rectangle
  stroke(255);
  noFill();
  rect(rectLeft,rectTop,rectWidth,rectHeight);
  
  // test ball
  //.drawBall(rectLeft,rectTop,ballDiameter,ballDiameter);
  drawBall(width/2,height/2,"test");
}


// function to draw a ball at a specific position
// this function takes into account the ball index
// we can use the index to start animating stuff
// we could also use other parameters to control how the ball looks
function drawBall(x,y,index) {
  ellipseMode(CENTER);
  ellipse(x,y,ballDiameter,ballDiameter);
  textAlign(CENTER, CENTER);
  text(index,x,y);
}