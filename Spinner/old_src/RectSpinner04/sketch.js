var rectLeft, rectTop, rectWidth, rectHeight;
var rectRight, rectBottom;
var ballDiameter;
var ballsHorizontal;
var ballsVertical;
var ballsTotal;
var animationDuration = 1; // time in seconds
var ballScaleRange = 10;
var ballScaleMin = 0.1;

var startTimeMs;

function setup() {
  createCanvas(400, 600);
  
  // initialize rectangle area for spinner
  rectWidth = width/2;
  rectHeight = height/2;
  rectLeft = width/2 - rectWidth/2;
  rectTop = height/2 - rectHeight/2;
  rectRight = rectLeft+rectWidth;
  rectBottom = rectTop+rectHeight;
  
  // initialize ball size 
  ballDiameter = min(rectWidth,rectHeight)*0.10;
  
  // initialize horizontal/vertical amount of balls
  ballsHorizontal = floor(rectWidth/ballDiameter);
  ballsVertical = floor(rectHeight/ballDiameter);
  ballsTotal = 2*ballsHorizontal+2*ballsVertical-4;
  
  startTimeMs=millis();
}

function draw() {
  background(0);
  
  // my own function for drawing the test rectangle and a test ball
  //testDrawRectAndBall();
  
  // create animation tween
  // result = values ranging from 0 to 1 (animPercentage)
  let deltaTimeSeconds = (millis()-startTimeMs)/1000.0; // time since start
  deltaTimeSeconds %= animationDuration; // make sure time clips within duration range
  let timePercentage = deltaTimeSeconds / animationDuration; // get values 0..1 that relate to the animation time
  let animPercentage = EasingFunctions.easeInOutQuad(timePercentage);
  
  
  // test the animation tween
  //testDrawAnimation(10,30,width-20,animPercentage);
  
  var animatedBallIndex = round(animPercentage*(ballsTotal-1));
  //console.log(animatedBallIndex);
  
  // draw all balls clockwise
  var ballIndex=0;
  // draw balls top row (left to right)
  for (var i=0;i<ballsHorizontal;i++) {
    let x = map(i,0,ballsHorizontal-1, rectLeft, rectRight);
    drawBall(x,rectTop,ballIndex,animatedBallIndex);
    ballIndex++;
  }
  
  // draw balls right side (top to bottom)
  for (i=1;i<ballsVertical;i++) {
    let y = map(i,0,ballsVertical-1, rectTop, rectBottom);
    drawBall(rectRight,y,ballIndex,animatedBallIndex);
    ballIndex++;
  }
  
  // draw balls bottom row (right to left)
  for (var i=ballsHorizontal-2;i>=0;i--) {
    let x = map(i,0,ballsHorizontal-1, rectLeft, rectRight);
    drawBall(x,rectBottom,ballIndex,animatedBallIndex);
    ballIndex++;
  }
  
  // draw balls left side (bottom to top)
  for (i=ballsVertical-2;i>0;i--) {
    let y = map(i,0,ballsVertical-1, rectTop, rectBottom);
    drawBall(rectLeft,y,ballIndex,animatedBallIndex);
    ballIndex++;
  }
  
}


// test for drawing ball and rect
function testDrawRectAndBall() {
  // test rectangle
  stroke(255);
  noFill();
  rect(rectLeft,rectTop,rectWidth,rectHeight);
  
  // test ball
  //.drawBall(rectLeft,rectTop,ballDiameter,ballDiameter);
  drawBall(width/2,height/2,ballDiameter,ballDiameter);
}

function testDrawAnimation(x,y,w,perc) {
  stroke(255);
  line(x,y,x+w,y);
  var xoff = w*perc;
  line(xoff,y-10,xoff,y+10);
}

// function to draw a ball at a specific position
// this function takes into account the ball index
// we can use the index to start animating stuff
// we could also use other parameters to control how the ball looks
function drawBall(x,y,ballIndex,focusBallIndex) {
  
  // calc if ball is focus ball
  // let isFocusBall = ballIndex==focusBallIndex;
  
  
  //let ballScale=ballScaleMin;
  let focusPercentage=0;
  
  // find the balls that are "behind this ball"
  if (ballIndex<=focusBallIndex && ballIndex>=focusBallIndex-ballScaleRange) {
    focusPercentage=map(ballIndex,focusBallIndex,focusBallIndex-ballScaleRange,1,0);
  }
  // also the ones that are in numnber larger, but still behind (around zero point)
  if (ballIndex-ballsTotal<=focusBallIndex && ballIndex-ballsTotal>=focusBallIndex-ballScaleRange) {
    focusPercentage=map(ballIndex-ballsTotal,focusBallIndex,focusBallIndex-ballScaleRange,1,0);
  }
  
  let ballScale = map(focusPercentage,0,1,ballScaleMin,1);
  
  let ballBright = map(focusPercentage, 0, 1, 64, 255);
  
//   noStroke();
//   if (isFocusBall) {
//     fill(255);
//   } else {
//     fill(64);
//   }
  
  noStroke();
  fill(ballBright);
  
  ellipseMode(CENTER);
  ellipse(x,y,ballDiameter*ballScale,ballDiameter*ballScale);
}